import mongoose, { Schema } from "mongoose";
import { ObjectId } from "mongodb";
import Contact from "../services/contacts/model";

const data = [
  {
    _id: new ObjectId("5d74e10b34e5923bf5b04fed"),
    isDeleted: false,
    name: "David John",
    email: [
      {
        _id: new ObjectId("5d74e10b34e5923bf5b04fef"),
        email: "david@work.com",
        tag: "work"
      },
      {
        _id: new ObjectId("5d74e10b34e5923bf5b04fee"),
        email: "david@john.com",
        tag: "personal"
      }
    ],
    phone: [
      {
        _id: new ObjectId("5d74e10b34e5923bf5b04ff0"),
        phone: "0987654321",
        tag: "work"
      }
    ]
  },
  {
    _id: new ObjectId("5d74e10b34e5923bf5b04fe1"),
    isDeleted: false,
    name: "Amarnath Thillai",
    email: [
      {
        _id: new ObjectId("5d74e10b34e5923bf5b04fe3"),
        email: "amar@work.com",
        tag: "work"
      },
      {
        _id: new ObjectId("5d74e10b34e5923bf5b04fe2"),
        email: "amar@thillai.com",
        tag: "personal"
      }
    ],
    phone: [
      {
        _id: new ObjectId("5d74e10b34e5923bf5b04fe4"),
        phone: "8190000393",
        tag: "work"
      }
    ]
  },
  {
    _id: new ObjectId("5d74e10b34e5923bf5b04fe5"),
    isDeleted: false,
    name: "Jon Deo",
    email: [
      {
        _id: new ObjectId("5d74e10b34e5923bf5b04fe7"),
        email: "amar@office.com",
        tag: "work"
      },
      {
        _id: new ObjectId("5d74e10b34e5923bf5b04fe6"),
        email: "amar@deo.com",
        tag: "personal"
      }
    ],
    phone: [
      {
        _id: new ObjectId("5d74e10b34e5923bf5b04fe8"),
        phone: "123456789",
        tag: "personal"
      }
    ]
  },
  {
    _id: new ObjectId("5d74e10b34e5923bf5b04fe9"),
    isDeleted: false,
    name: "Michal Dave",
    email: [
      {
        _id: new ObjectId("5d74e10b34e5923bf5b04feb"),
        email: "amar@work.com",
        tag: "work"
      },
      {
        _id: new ObjectId("5d74e10b34e5923bf5b04fea"),
        email: "amar@deo.com",
        tag: "personal"
      }
    ],
    phone: [
      {
        _id: new ObjectId("5d74e10b34e5923bf5b04fec"),
        phone: "111111111",
        tag: "personal"
      }
    ]
  }
];

export default class ContactsSeeder {
  async run() {
    const mongoURL = process.env.MONGO_DB_URI;
    await mongoose.connect(mongoURL, { useNewUrlParser: true });
    console.log("conneted to " + mongoURL);
    mongoose.connection.db.dropCollection("contacts");
    console.log("Seeding contacts");

    return Contact.create(data);
  }
}
