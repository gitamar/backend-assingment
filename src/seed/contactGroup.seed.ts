import mongoose from "mongoose";
import { ObjectId } from "mongodb";
import ContactGroup from "../services/contactGroups/model";

const data = [
  {
    _id: new ObjectId("5d74e43b04856244380a7182"),
    contacts: [
      new ObjectId("5d74e10b34e5923bf5b04fed"),
      new ObjectId("5d74e10b34e5923bf5b04fe9"),
      new ObjectId("5d74e10b34e5923bf5b04fe5")
    ],
    isDeleted: false,
    name: "Friends Group"
  },
  {
    _id: new ObjectId("5d74e49104856244380a7183"),
    contacts: [
      new ObjectId("5d74e10b34e5923bf5b04fe1"),
      new ObjectId("5d74e10b34e5923bf5b04fe5")
    ],
    isDeleted: false,
    name: "Office Group"
  }
];

export default class ContactGroupsSeeder {
  async run() {
    const mongoURL = process.env.MONGO_DB_URI;
    await mongoose.connect(mongoURL, { useNewUrlParser: true });
    console.log("conneted to " + mongoURL);
    mongoose.connection.db.dropCollection("contactgroups");
    console.log("Seeding contactgroups");
    return ContactGroup.create(data);
  }
}
