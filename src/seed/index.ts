import ContactsSeeder from "./contact.seed";
import ContactGroupsSeeder from "./contactGroup.seed";
export default { ContactGroupsSeeder, ContactsSeeder };
