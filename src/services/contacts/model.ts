import mongoose from "../../utils/mongoConnection";
import { Schema, Document } from "mongoose";

export interface IEmail extends Document {
  email: string;
  tag: string;
}

export interface IPhone extends Document {
  phone: string;
  tag: string;
}
export interface IContact extends Document {
  name: string;
  email: Array<IEmail>;
  phone: Array<IPhone>;
}

const EmailSchema: Schema = new Schema({
  email: { type: String, required: true },
  tag: { type: String, required: true }
});

const PhoneSchema: Schema = new Schema({
  phone: { type: String, required: true },
  tag: { type: String, required: true }
});

export const ContactSchema: Schema = new Schema(
  {
    name: { type: String, required: true, unique: true },
    email: { type: [EmailSchema], required: true, unique: true },
    phone: { type: [PhoneSchema], required: true, unique: true },
    isDeleted: { type: Boolean, default: false }
  },
  { timestamps: true }
);

ContactSchema.index({ "$**": "text" });

export default mongoose.model<IContact>("Contact", ContactSchema);
