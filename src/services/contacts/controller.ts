import { Response } from "express";
import { ObjectId } from "mongodb";
import ContactSchema, { IContact } from "./model";

export const getContact = async (res: Response, page: number) => {
  try {
    let perPage = 10,
      pageNo = Math.max(0, page);
    let result = await ContactSchema.find({ isDeleted: false })
      .select("name")
      .limit(perPage)
      .skip(perPage * pageNo)
      .sort({
        name: "asc"
      });
    return { succes: true, message: `Contact list`, result: result };
  } catch (error) {
    throw "Error on get category" + error;
  }
};

export const getContactDetail = async (res: Response, id: ObjectId) => {
  try {
    let result = await ContactSchema.findOne({ _id: id, isDeleted: false });
    if (result == null) {
      res.status(404).send({
        succes: false,
        message: `No contact assosiated with id : ${id} `
      });
    }
    return { succes: true, message: `Contact detail found`, result: result };
  } catch (error) {
    throw "Error on get contact " + error;
  }
};

export const updateContact = async (
  res: Response,
  body: IContact,
  id: ObjectId
) => {
  try {
    let isIdExists = await ContactSchema.findOne({ _id: id, isDeleted: false });
    if (isIdExists == null) {
      res.status(404).send({
        succes: false,
        message: `No contact assosiated with id : ${id} `
      });
    }
    let result = await ContactSchema.findByIdAndUpdate(id, body, { new: true });
    return { succes: true, message: `Contact updated`, result: result };
  } catch (error) {
    if (error.name == "MongoError" && error.code === 11000) {
      res.status(409).send({
        succes: false,
        message: `Contact name ${body.name} alredy exists `
      });
    } else {
      throw error;
    }
  }
};

export const deleteContact = async (res: Response, id: ObjectId) => {
  try {
    let isIdExists = await ContactSchema.findOne({ _id: id, isDeleted: false });
    if (isIdExists == null) {
      res.status(404).send({
        succes: false,
        message: `No contact assosiated with id : ${id} `
      });
    } else {
      let result = await ContactSchema.findByIdAndUpdate(
        id,
        { isDeleted: true },
        { new: true }
      );
      return { succes: true, message: `Contact deleted`, result: result };
    }
  } catch (error) {
    throw error;
  }
};

export const createContact = async (res: Response, body: IContact) => {
  try {
    let result = await ContactSchema.create(body);
    return { succes: true, message: `Contact created`, result: result };
  } catch (error) {
    if (error.name == "MongoError" && error.code === 11000) {
      res.status(409).send({
        succes: false,
        message: `Contact name ${body.name} alredy exists `
      });
    } else {
      throw error;
    }
  }
};

export const searchContact = async (
  res: Response,
  key: String,
  page: number
) => {
  try {
    let perPage = 10,
      pageNo = Math.max(0, page);
    let result = await ContactSchema.find({ $text: { $search: key } })
      .select("name email phone")
      .limit(perPage)
      .skip(perPage * pageNo)
      .sort("-createdAt");
    return { succes: true, message: `Contact search list`, result: result };
  } catch (error) {
    throw error;
  }
};
