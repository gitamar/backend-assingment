import Joi from "@hapi/joi";

const emailObjectSchema = Joi.object({
  email: Joi.string()
    .email()
    .required(),
  tag: Joi.alternatives()
    .try("work", "personal")
    .required()
}).required();

const emailArraySchema = Joi.array()
  .items(emailObjectSchema)
  .min(1)
  .unique()
  .required();

const phoneObjectSchema = Joi.object({
  phone: Joi.string().required(),
  tag: Joi.alternatives()
    .try("work", "personal")
    .required()
}).required();

const phoneArraySchema = Joi.array()
  .items(phoneObjectSchema)
  .min(1)
  .unique()
  .required();

export const contactPOST = Joi.object().keys({
  name: Joi.string()
    .min(3)
    .max(20)
    .required(),
  email: emailArraySchema,
  phone: phoneArraySchema
});

export const contactLIST = Joi.object().keys({
  page: Joi.number(),
  pageSize: Joi.number(),
  q: Joi.string()
    .min(3)
    .max(20)
});

export const contactDETAIL = Joi.object().keys({
  id: Joi.string()
    .min(24)
    .max(24)
});

export const contactDELETE = Joi.object().keys({
  id: Joi.string()
    .min(24)
    .max(24)
});
