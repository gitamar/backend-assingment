import { Request, Response } from "express";
import { ObjectId } from "mongodb";
import { validate } from "../../middleware/validationHandler";
import {
  contactPOST,
  contactDETAIL,
  contactDELETE,
  contactLIST
} from "./schema";
import {
  getContact,
  createContact,
  getContactDetail,
  updateContact,
  deleteContact,
  searchContact
} from "./controller";

export default [
  {
    path: "/api/v1/contacts",
    method: "get",
    handler: [
      validate(contactLIST, "query"),
      async (req: Request, res: Response) => {
        let response;
        if (req.query.q !== undefined) {
          response = await searchContact(res, req.query.q, req.query.page);
        } else {
          response = await getContact(res, req.query.page);
        }
        res.status(200).json(response);
      }
    ]
  },
  {
    path: "/api/v1/contacts/:id",
    method: "get",
    handler: [
      validate(contactDETAIL, "params"),
      async (req: Request, res: Response) => {
        let response = await getContactDetail(res, new ObjectId(req.params.id));
        res.status(200).json(response);
      }
    ]
  },
  {
    path: "/api/v1/contacts/:id",
    method: "put",
    handler: [
      validate(contactPOST, "body"),
      validate(contactDETAIL, "params"),
      async (req: Request, res: Response) => {
        let response = await updateContact(
          res,
          req.body,
          new ObjectId(req.params.id)
        );
        res.status(200).json(response);
      }
    ]
  },
  {
    path: "/api/v1/contacts",
    method: "post",
    handler: [
      validate(contactPOST, "body"),
      async (req: Request, res: Response) => {
        let response = await createContact(res, req.body);
        res.status(200).json(response);
      }
    ]
  },
  {
    path: "/api/v1/contacts/:id",
    method: "delete",
    handler: [
      validate(contactDELETE, "params"),
      async (req: Request, res: Response) => {
        let response = await deleteContact(res, new ObjectId(req.params.id));
        res.status(200).json(response);
      }
    ]
  }
];
