import contactGroupsRoutes from "./contactGroups/routes";
import contactsRoutes from "./contacts/routes";

export default [...contactsRoutes, ...contactGroupsRoutes];
