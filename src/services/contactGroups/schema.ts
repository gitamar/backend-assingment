import Joi from "@hapi/joi";

const contactId = Joi.string()
  .min(24)
  .max(24);

const contactIdArray = Joi.array()
  .items(contactId)
  .min(1)
  .unique()
  .required();

export const contactGroupPOST = Joi.object().keys({
  name: Joi.string()
    .min(3)
    .max(20)
    .required(),
  contacts: contactIdArray
});

export const contactGroupDETAIL = Joi.object().keys({
  id: Joi.string()
    .min(24)
    .max(24)
});
