import mongoose from "../../utils/mongoConnection";
import { Schema, Document } from "mongoose";

export interface IContactGroup extends Document {
  name: string;
  contacts: Array<Schema.Types.ObjectId>;
}

export const ContactGroupSchema: Schema = new Schema(
  {
    name: { type: String, required: true, unique: true },
    contacts: {
      type: [{ type: Schema.Types.ObjectId, ref: "Contact" }],
      required: true
    },
    isDeleted: { type: Boolean, default: false }
  },
  { timestamps: true }
);

export default mongoose.model<IContactGroup>(
  "ContactGroup",
  ContactGroupSchema
);
