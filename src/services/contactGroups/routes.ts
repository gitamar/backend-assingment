import { Request, Response } from "express";
import { ObjectId } from "mongodb";
import { validate } from "../../middleware/validationHandler";
import { contactGroupPOST, contactGroupDETAIL } from "./schema";
import {
  createContactGroup,
  getContactGroup,
  getContactGroupDetail,
  editContactGroup,
  deleteContactGroupDetail
} from "./controller";

export default [
  {
    path: "/api/v1/contactGroup",
    method: "post",
    handler: [
      validate(contactGroupPOST, "body"),
      async (req: Request, res: Response) => {
        req.body.contacts = req.body.contacts.map(
          (id: string) => new ObjectId(id)
        );
        let response = await createContactGroup(res, req.body);
        res.status(200).json(response);
      }
    ]
  },
  {
    path: "/api/v1/contactGroup",
    method: "get",
    handler: [
      // validate(contactPOST, "body"),
      async (req: Request, res: Response) => {
        let response = await getContactGroup();
        res.status(200).json(response);
      }
    ]
  },
  {
    path: "/api/v1/contactGroup/:id",
    method: "get",
    handler: [
      validate(contactGroupDETAIL, "params"),
      async (req: Request, res: Response) => {
        let response = await getContactGroupDetail(
          res,
          new ObjectId(req.params.id)
        );
        res.status(200).json(response);
      }
    ]
  },
  {
    path: "/api/v1/contactGroup/:id",
    method: "put",
    handler: [
      validate(contactGroupPOST, "body"),
      validate(contactGroupDETAIL, "params"),
      async (req: Request, res: Response) => {
        let response = await editContactGroup(
          res,
          new ObjectId(req.params.id),
          req.body
        );
        res.status(200).json(response);
      }
    ]
  },
  {
    path: "/api/v1/contactGroup/:id",
    method: "delete",
    handler: [
      validate(contactGroupDETAIL, "params"),
      async (req: Request, res: Response) => {
        let response = await deleteContactGroupDetail(
          res,
          new ObjectId(req.params.id)
        );
        res.status(200).json(response);
      }
    ]
  }
];
