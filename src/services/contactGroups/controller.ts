import { Response } from "express";
import { ObjectId, ObjectID } from "mongodb";
import ContactGroupSchema, { IContactGroup } from "./model";
import ContactSchema from "../contacts/model";

export const createContactGroup = async (
  res: Response,
  body: IContactGroup
) => {
  try {
    let isExists: boolean[] = [];
    for (const id of body.contacts) {
      let count = await ContactSchema.findOne({ _id: id })
        .select("_id")
        .lean();
      if (count == null) {
        isExists.push(false);
      } else {
        isExists.push(true);
      }
    }

    if (isExists.includes(false)) {
      res.status(409).send({
        succes: false,
        message: `Some of the contact ids does not exist `
      });
    } else {
      let result = await ContactGroupSchema.create(body);
      return { succes: true, message: `Contact Group created`, result: result };
    }
  } catch (error) {
    if (error.name == "MongoError" && error.code === 11000) {
      res.status(409).send({
        succes: false,
        message: `Contact Group name "${body.name}" alredy exists `
      });
    } else {
      throw error;
    }
  }
};

export const editContactGroup = async (
  res: Response,
  id: ObjectID,
  body: IContactGroup
) => {
  try {
    let isExist = await ContactGroupSchema.findByIdAndUpdate(id, {
      isDeleted: true
    });
    if (isExist == null) {
      res.status(404).send({
        succes: false,
        message: `No contact assosiated with id : ${id} `
      });
    } else {
      let result = await ContactGroupSchema.findByIdAndUpdate(id, body, {
        new: true
      });
      return { succes: true, message: `Contact Group updated`, result: result };
    }
  } catch (error) {
    if (error.name == "MongoError" && error.code === 11000) {
      res.status(409).send({
        succes: false,
        message: `Contact Group name "${body.name}" alredy exists `
      });
    } else {
      throw error;
    }
  }
};

export const getContactGroup = async () => {
  try {
    let result = await ContactGroupSchema.find({}).select("name");
    return { succes: true, message: `Contact group list`, result: result };
  } catch (error) {
    throw error;
  }
};

export const getContactGroupDetail = async (res: Response, id: ObjectID) => {
  try {
    let result = await ContactGroupSchema.findOne({
      _id: id,
      isDeleted: false
    }).populate("contacts");
    if (result == null) {
      res.status(404).send({
        succes: false,
        message: `No contact assosiated with id : ${id} `
      });
    } else {
      return { succes: true, message: `Contact group list`, result: result };
    }
  } catch (error) {
    throw error;
  }
};

export const deleteContactGroupDetail = async (res: Response, id: ObjectID) => {
  try {
    let result = await ContactGroupSchema.findByIdAndUpdate(id, {
      isDeleted: true
    });
    if (result == null) {
      res.status(404).send({
        succes: false,
        message: `No contact assosiated with id : ${id} `
      });
    } else {
      return { succes: true, message: `Contact group deleted`, result: result };
    }
  } catch (error) {
    throw error;
  }
};
