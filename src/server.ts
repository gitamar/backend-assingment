import http from "http";
import express from "express";
import { applyMiddleware, applyRoutes } from "./utils";
import seed from "./seed";
import middleware from "./middleware";
import routes from "./services";
import dotenv from "dotenv";

dotenv.config();
declare global {
  namespace NodeJS {
    interface ProcessEnv {
      MONGO_DB_URI: string;
      MONGO_DB_TEST_URI: string;
      SEED_ENV: string;
    }
  }
}

process.on("uncaughtException", e => {
  console.log(e);
  process.exit(1);
});

process.on("unhandledRejection", e => {
  console.log(e);
  process.exit(1);
});

const router = express();
applyMiddleware(middleware, router);
applyRoutes(routes, router);

const { PORT = 3000 } = process.env;
export const server = http.createServer(router);

if (process.env.SEED_ENV == "true") {
  const contactSeeder = new seed.ContactsSeeder();
  const contactGroupsSeeder = new seed.ContactGroupsSeeder();
  (async () => {
    await contactSeeder.run();
    await contactGroupsSeeder.run();
    console.log("Seeding complete");
  })();
} else {
  if (!module.parent) {
    server.listen(PORT, () => {
      console.log(`Server is running http://localhost:${PORT}...`);
    });
  }
}
