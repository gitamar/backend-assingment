
import Joi,{JoiObject} from '@hapi/joi'
import { Request, Response, NextFunction } from 'express';

interface Req extends Request {
    [property:string]: any; 
}

export const validate = (schema: JoiObject, property:string) => {
  return (req:Req, res:Response, next:NextFunction) => {

    const { error } = Joi.validate(req[property], schema);

    const valid = error == null;
    if (valid) {
      next();
    } else {
      
      const { details } = error;
      const message = details.map(i => i.message).join(',');
      console.log("error", message);

      res.status(422).json({
        error: message 
      })
    }
  }
}
