import { Router } from 'express';
import cors from 'cors';
import parser from 'body-parser';
import compression from 'compression';

export const handleCors = (router: Router) => {
  router.use(
    cors({
      origin: '*',
      allowedHeaders: 'Origin, Content-Type, X-Auth-Token, content-type',
      methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
    })
  );
};
export const handleBodyRequestParsing = (router: Router) => {
  router.use(parser.urlencoded({ extended: true }));
  router.use(parser.json());
};

export const handleCompression = (router: Router) => {
  router.use(compression());
};
