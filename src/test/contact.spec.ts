import chai from "chai";
import chaiHttp from "chai-http";
import { server } from "../server";

chai.use(chaiHttp);
const apiBase = "/api/v1";
const should = chai.should();

let objectId: string;

describe("/POST contacts", () => {
  it("it should not POST a contact without email", done => {
    let contact = {
      name: "Amarnath Thillai",
      phone: [{ phone: "098765432", tag: "work" }]
    };
    chai
      .request(server)
      .post(`${apiBase}/contacts`)
      .send(contact)
      .end((err, res) => {
        res.should.have.status(422);
        res.body.should.be.a("object");
        res.body.error.should.be.eql(`"email" is required`);
        done();
      });
  });

  it("it should not POST a contact without phone", done => {
    let contact = {
      name: "Amarnath Thillai",
      email: [
        { email: "jon@work.com", tag: "work" },
        { email: "jon@deo.com", tag: "personal" }
      ]
    };
    chai
      .request(server)
      .post(`${apiBase}/contacts`)
      .send(contact)
      .end((err, res) => {
        res.should.have.status(422);
        res.body.should.be.a("object");
        res.body.error.should.be.eql(`"phone" is required`);
        done();
      });
  });

  it("it should not POST a contact without email tag", done => {
    let contact = {
      name: "Amarnath Thillai",
      email: [
        { email: "jon@work.com" },
        { email: "jon@deo.com", tag: "personal" }
      ],
      phone: [{ phone: "098765432", tag: "work" }]
    };
    chai
      .request(server)
      .post(`${apiBase}/contacts`)
      .send(contact)
      .end((err, res) => {
        res.should.have.status(422);
        res.body.should.be.a("object");
        res.body.error.should.be.eql(`"tag" is required`);
        done();
      });
  });
  it("it should not POST a contact without phone tag", done => {
    let contact = {
      name: "Amarnath Thillai",
      email: [
        { email: "jon@work.com", tag: "work" },
        { email: "jon@deo.com", tag: "personal" }
      ],
      phone: [{ phone: "098765432" }]
    };
    chai
      .request(server)
      .post(`${apiBase}/contacts`)
      .send(contact)
      .end((err, res) => {
        res.should.have.status(422);
        res.body.should.be.a("object");
        res.body.error.should.be.eql(`"tag" is required`);
        done();
      });
  });

  it("it should not POST a contact without tags other than work or personal", done => {
    let contact = {
      name: "Amarnath Thillai",
      email: [
        { email: "jon@work.com", tag: "work" },
        { email: "jon@deo.com", tag: "personal" }
      ],
      phone: [{ phone: "098765432", tag: "office" }]
    };
    chai
      .request(server)
      .post(`${apiBase}/contacts`)
      .send(contact)
      .end((err, res) => {
        res.should.have.status(422);
        res.body.should.be.a("object");
        res.body.error.should.be.eql(
          `"tag" must be one of [work],"tag" must be one of [personal]`
        );
        done();
      });
  });

  it("it should not POST a contact with duplicate entries", done => {
    let contact = {
      name: "Amarnath Thillai",
      email: [
        { email: "jon@work.com", tag: "work" },
        { email: "jon@deo.com", tag: "personal" },
        { email: "jon@deo.com", tag: "personal" }
      ],
      phone: [{ phone: "098765432", tag: "office" }]
    };
    chai
      .request(server)
      .post(`${apiBase}/contacts`)
      .send(contact)
      .end((err, res) => {
        res.should.have.status(422);
        res.body.should.be.a("object");
        res.body.error.should.be.eql(
          `"email" position 2 contains a duplicate value`
        );
        done();
      });
  });

  it("it should POST a contact", done => {
    let contact = {
      name: "Amarnath Thillai",
      email: [
        { email: "jon@work.com", tag: "work" },
        { email: "jon@deo.com", tag: "personal" }
      ],
      phone: [{ phone: "098765432", tag: "work" }]
    };
    chai
      .request(server)
      .post(`${apiBase}/contacts`)
      .send(contact)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.result.should.be.a("object");
        process.env.contactId = res.body.result._id;
        objectId = res.body.result._id;

        done();
      });
  });
  it("it should not POST a contact with existing name", done => {
    let contact = {
      name: "Amarnath Thillai",
      email: [
        { email: "jon@work.com", tag: "work" },
        { email: "jon@deo.com", tag: "personal" }
      ],
      phone: [{ phone: "098765432", tag: "work" }]
    };
    chai
      .request(server)
      .post(`${apiBase}/contacts`)
      .send(contact)
      .end((err, res) => {
        res.should.have.status(409);
        res.body.should.be.a("object");
        res.body.succes.should.be.eql(false);
        res.body.message.should.be.eql(
          `Contact name ${contact.name} alredy exists `
        );
        done();
      });
  });
});

describe("/GET contacts", () => {
  it("it should GET all the contacts", done => {
    chai
      .request(server)
      .get(`${apiBase}/contacts`)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.result.should.be.a("array");
        res.body.result.length.should.be.eql(1);
        done();
      });
  });
  it("it should GET all the contacts matching amarnath", done => {
    chai
      .request(server)
      .get(`${apiBase}/contacts/?q=amarnath`)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.result.should.be.a("array");
        res.body.result.length.should.be.eql(1);
        res.body.result[0].name.should.be.eql("Amarnath Thillai");
        done();
      });
  });
  it("it should not GET contacts without search term more than three chars", done => {
    chai
      .request(server)
      .get(`${apiBase}/contacts/?q=am`)
      .end((err, res) => {
        res.should.have.status(422);
        res.body.error.should.be.a("string");
        res.body.error.should.be.eql(
          `"q" length must be at least 3 characters long`
        );
        done();
      });
  });
});

describe("/GET show a contact details", () => {
  it(`it should GWT the contact details with objectID : ${objectId}`, done => {
    chai
      .request(server)
      .get(`${apiBase}/contacts/${objectId}`)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
});

describe("/DELETE contacts", () => {
  it(`it should DELETE the contact with objectID : ${objectId}`, done => {
    chai
      .request(server)
      .delete(`${apiBase}/contacts/${objectId}`)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.message.should.be.eql("Contact deleted");
        done();
      });
  });
  it("it should not DELETE no objectid is passed", done => {
    chai
      .request(server)
      .delete(`${apiBase}/contacts/deletethis`)
      .end((err, res) => {
        res.should.have.status(422);
        res.body.error.should.be.eql(
          '"id" length must be at least 24 characters long'
        );
        done();
      });
  });
  it("it should not DELETE wrong objectid is passed", done => {
    chai
      .request(server)
      .delete(`${apiBase}/contacts/5d72b14ec1ecaf70982717f7`)
      .end((err, res) => {
        res.should.have.status(404);
        res.body.succes.should.be.eql(false);
        res.body.message.should.be.eql(
          "No contact assosiated with id : 5d72b14ec1ecaf70982717f7 "
        );
        done();
      });
  });
});
