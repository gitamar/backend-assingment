import chai from "chai";
import chaiHttp from "chai-http";
import { server } from "../server";

chai.use(chaiHttp);
const apiBase = "/api/v1";
const should = chai.should();

let objectId: string;

describe("/POST contact Group", () => {
  it("it should not POST a contact group without contact ids", done => {
    let contactGroup = {
      name: "Friends Group"
    };
    chai
      .request(server)
      .post(`${apiBase}/contactGroup`)
      .send(contactGroup)
      .end((err, res) => {
        res.should.have.status(422);
        res.body.should.be.a("object");
        res.body.error.should.be.eql(`"contacts" is required`);
        done();
      });
  });

  it("it should not POST a contact group without valid contact id", done => {
    let contact = {
      name: "Friends Group",
      contacts: ["not a valid object id"]
    };
    chai
      .request(server)
      .post(`${apiBase}/contactGroup`)
      .send(contact)
      .end((err, res) => {
        res.should.have.status(422);
        res.body.should.be.a("object");
        res.body.error.should.be.eql(
          `"0" length must be at least 24 characters long`
        );
        done();
      });
  });

  it("it should not POST a contact group without valid un assosiated contact id", done => {
    let contact = {
      name: "Friends Group",
      contacts: ["5d73fcd1929bf34b656f3671"]
    };
    chai
      .request(server)
      .post(`${apiBase}/contactGroup`)
      .send(contact)
      .end((err, res) => {
        res.should.have.status(409);
        res.body.should.be.a("object");
        res.body.message.should.be.eql(
          `Some of the contact ids does not exist `
        );
        done();
      });
  });

  it("it should  POST a contact group", done => {
    let contact = {
      name: "Friends Group",
      contacts: [`${process.env.contactId}`]
    };
    chai
      .request(server)
      .post(`${apiBase}/contactGroup`)
      .send(contact)
      .end((err, res) => {
        res.should.have.status(200);
        objectId = res.body.result._id;
        res.body.should.be.a("object");
        res.body.message.should.be.eql(`Contact Group created`);
        done();
      });
  });

  it("it should not POST a contact group with duplicate name", done => {
    let contact = {
      name: "Friends Group",
      contacts: [`${process.env.contactId}`]
    };
    chai
      .request(server)
      .post(`${apiBase}/contactGroup`)
      .send(contact)
      .end((err, res) => {
        res.should.have.status(409);
        res.body.should.be.a("object");
        res.body.message.should.be.eql(
          `Contact Group name "Friends Group" alredy exists `
        );
        done();
      });
  });
});

describe("/GET show a contact group details", () => {
  it(`it should GET the contact group details with objectID `, done => {
    chai
      .request(server)
      .get(`${apiBase}/contactGroup/${objectId}`)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
});

describe("/GET show a contact group list", () => {
  it(`it should GET the contact group list `, done => {
    chai
      .request(server)
      .get(`${apiBase}/contactGroup`)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.result.length.should.be.eql(1);
        done();
      });
  });
});

describe("/DELETE contact group", () => {
  it(`it should DELETE the contact group with objectID `, done => {
    chai
      .request(server)
      .delete(`${apiBase}/contactGroup/${objectId}`)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.message.should.be.eql("Contact group deleted");
        done();
      });
  });
  it("it should not DELETE no objectid is passed", done => {
    chai
      .request(server)
      .delete(`${apiBase}/contactGroup/deletethis`)
      .end((err, res) => {
        res.should.have.status(422);
        res.body.error.should.be.eql(
          '"id" length must be at least 24 characters long'
        );
        done();
      });
  });
  it("it should not DELETE wrong objectid is passed", done => {
    chai
      .request(server)
      .delete(`${apiBase}/contactGroup/5d72b14ec1ecaf70982717f7`)
      .end((err, res) => {
        res.should.have.status(404);
        res.body.succes.should.be.eql(false);
        res.body.message.should.be.eql(
          "No contact assosiated with id : 5d72b14ec1ecaf70982717f7 "
        );
        done();
      });
  });
});
