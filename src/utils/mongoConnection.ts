import mongoose from "mongoose";
import dotenv from "dotenv";

dotenv.config();

mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);

mongoose
  .connect(
    process.env.NODE_ENV == "test"
      ? process.env.MONGO_DB_TEST_URI
      : process.env.MONGO_DB_URI
  )
  // .connect("mongodb://localhost:27017/backend-assingment-test")
  .then(() => {
    return console.info(
      `Successfully connected to ${
        process.env.NODE_ENV == "test"
          ? process.env.MONGO_DB_TEST_URI
          : process.env.MONGO_DB_URI
      }`,
      process.env.NODE_ENV
    );
  })
  .catch(error => {
    console.error("Error connecting to database: ", error);
    return process.exit(1);
  });

export default mongoose;
