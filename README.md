# Backend Assingment - Contact Book

REST API for a contacts app. With basic contacts management , Contatc groups , and Contact search.

After successfully running the project a swagger doc will be ready on http://localhost:3000/api-docs/#/

---

## Requirements

For development, you will need `Linux or Unix based OS` `Node.js v12+` , `Typescript`, `MongoDB v4+` and a node global package, `npm`, installed in your environement.

---

## Application Structure

    .
    ├── api.http
    ├── Backend assignment.pdf
    ├── package.json
    ├── pm2.json
    ├── README.md
    ├── src
    │   ├── config
    │   ├── middleware
    │   ├── seed
    │   ├── server.ts
    │   ├── services
    │   ├── test
    │   └── utils
    └── tsconfig.json

- `api.http` - Vscode rest client file.
- `Backend assignment.pdf` - Requirement Spec of this project.
- `package.json` - Npm generated - Holds all the dependencies and start scripts.
- `pm2.json` - Pm2 config when running the app in prod.
- `README.md` - This README.md file.
- `src/` - This folder contains all the typescript source.
- `src/config` - Holds swagger config.
- `src/middleware` - Holds, Swagger , Validaion, Error , and other common middlewares like `cors` , `bodyparser` etc.
- `src/seed` - Holds the seeding data for mongodb.
- `./src/server.ts` - The entry point to our application. This file defines our express server and connects it to MongoDB using mongoose. It also requires the routes and models we'll be using in the application and apply middlewares to the routes.
- `src/services` - Holds all services with theri buissnes logic , model definition, and route definition.
- `src/test` - Holds all the test cases.
- `src/utils` - Holds client error classes, Mongoose connetion
- `tsconfig.json` - Holds the ts lint and transpile config.

---

## Install

    $ git clone https://github.com/YOUR_USERNAME/contactbook
    $ cd contactbook
    $ npm install

---

## Configure app

Open `.env` then edit it with your settings. You will need:

- Mongo db dev URI
- Mongo db test URI

---

## Seeding the project at dev

    $ npm run dev:seed

## Testing the project

Drop test database before running test

    $ npm run test

## Running the project at dev

    $ npm run dev

The api server will be running on http://localhost:3000 with base path `/api/v1`

## Running the project at prod

    $ npm start

This will start the api server as a pm2 process in cluster mode.
